//
//  PentagoViewController.m
//  PentagoStudentVersion
//
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import "PentagoViewController.h"
#import "PentagoSubBoardViewController.h"
#import "PentagoBrain.h"


@interface PentagoViewController ()
@property (nonatomic, strong) NSMutableArray *subViewControllers;
@property (nonatomic) UILabel * winLabel;
@property (nonatomic) UIButton * reset;



@end

@implementation PentagoViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  
    if (self) {


    }
    [[NSNotificationCenter defaultCenter] addObserverForName:@"winStatus" object:nil queue:nil usingBlock:^(NSNotification * notification){
        

        
        [self.winLabel setText:notification.object];
        
    }];




    return self;
}


-(NSMutableArray *) subViewControllers
{
    if( ! _subViewControllers )
        _subViewControllers = [[NSMutableArray alloc] initWithCapacity:4];
    return _subViewControllers;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor blackColor]];
    CGRect frame = [[UIScreen mainScreen] applicationFrame];
    [self.view setFrame:frame];
    
    // This is our root view-controller. Each of the quadrants of the game is
    // represented by a different view-controller. We create them here and add their views to the
    // view of the root view-controller.
    
    for (int i = 0; i < 4; i++) {
        PentagoSubBoardViewController *p = [[PentagoSubBoardViewController alloc] initWithSubsquare:i];
        [p.view setBackgroundColor:[UIColor blackColor]];
        [self.subViewControllers addObject: p];
        [self.view addSubview: p.view];
    }

    self.winLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 100)];
    [self.winLabel setText:@"Game In Progress"];
    [self.winLabel setTextColor:[UIColor greenColor]];
    [self.winLabel setCenter:CGPointMake(self.view.center.x, self.view.center.y*1.5)];
    [self.winLabel setTextAlignment:NSTextAlignmentCenter];
    [self.winLabel setFont:[UIFont boldSystemFontOfSize:20]];
    [self.view addSubview:self.winLabel];
    
    self.reset = [[UIButton alloc] initWithFrame:CGRectMake(0,0, 150, 75) ];
    
    [self.reset setBackgroundColor:[UIColor greenColor]];
    [self.reset.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
    [self.reset setTitle:@"RESET" forState:UIControlStateNormal];
    [self.reset setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.reset setCenter:CGPointMake(self.view.center.x, self.view.center.y*1.75)];
    [self.reset addTarget:self
                   action:@selector(resetGame:)
         forControlEvents:UIControlEventTouchDown];
    
    [self.view addSubview:self.reset];
    
}
-(void) resetGame: (UIButton *) button
{
    for (int i=0; i<4; i++)
    {
        [[[self.subViewControllers objectAtIndex:i] view] removeFromSuperview];
        [[self.subViewControllers objectAtIndex:i] killBrain];
        
        
    }
    self.subViewControllers = nil;
 
    for (int i = 0; i < 4; i++) {
        PentagoSubBoardViewController *p = [[PentagoSubBoardViewController alloc] initWithSubsquare:i];
        [p.view setBackgroundColor:[UIColor blackColor]];
        [self.subViewControllers addObject: p];
        [self.view addSubview: p.view];
    }
  
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
