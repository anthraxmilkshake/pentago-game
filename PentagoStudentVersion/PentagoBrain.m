//
//  PentagoBrain.m
//  PentagoStudentVersion
//
//  
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import "PentagoBrain.h"

@interface PentagoBrain()
@property (nonatomic) NSMutableArray * gameBoard;
@property (nonatomic) BOOL isP1Turn;
@property (nonatomic) BOOL rotateMode;
@property (nonatomic) BOOL gameWon;

@end

@implementation PentagoBrain


+(PentagoBrain *) sharedInstance
{
    static PentagoBrain *sharedObject = nil;
    
    if( sharedObject == nil )
    {
        sharedObject = [[PentagoBrain alloc] init];
        sharedObject.isP1Turn = true;
    }
    return sharedObject;
}
-(BOOL) gameIsWon
{
   
    return self.gameWon;
}
-(BOOL) isRotateMode
{
    return self.rotateMode;
}
-(void) resetBrain
{
    self.gameWon = false;
    self.isP1Turn = true;
    self.rotateMode = false;
    for (int i=0; i<6;i++)
        for (int j=0;j<6; j++) {
            self.gameBoard[i][j] = [NSNumber numberWithInt:0];
        }
}
-(NSMutableArray*) gameBoard
{
    if (!_gameBoard)
    {
        _gameBoard = [[NSMutableArray alloc]initWithCapacity:6];
        for (int i=0; i<6; i++)
            [_gameBoard addObject: [[NSMutableArray alloc]initWithCapacity:6]];
        for (int i=0; i<6;i++)
            for (int j=0;j<6; j++) {
                _gameBoard[i][j] = [NSNumber numberWithInt:0];
            }
        
       
    }
    return _gameBoard;
}

-(BOOL) placeWithX:(int)x AndY:(int)y InQuad:(int)q
{
    if (self.rotateMode || self.gameWon) {
        return false;
    }
    switch (q) {
        case 1:
            x+= 3;
            break;
        case 2:
            y+=3;
            break;
        case 3:
            x+=3;
            y+=3;
            break;
        default:
            break;
    }
    
    if (![self.gameBoard[x][y]  isEqual: @0]) {
        return false;
    }
    if (self.isP1Turn == true) {
         self.gameBoard[x][y] = [NSNumber numberWithInt:1];
    }
    else
        self.gameBoard[x][y] =[NSNumber numberWithInt:2];
    self.isP1Turn = !self.isP1Turn;
    self.rotateMode = true;
    return true;
}
-(BOOL) p1Turn
{
    
    return self.isP1Turn;
}
-(void) swipedDirection: (NSString*)direction inQuad:(int)quad;
{
    

    int xAdjust;
    int yAdjust;
    int xSourceAdjust;
    int ySourceAdjust;

    
    switch (quad) {
        case 0:
            xAdjust = 0;
            yAdjust = 2;
            xSourceAdjust = 0;
            ySourceAdjust = 0;

            break;
        case 1:
            xAdjust = -3;
            yAdjust = 5;
            xSourceAdjust = 3;
            ySourceAdjust = 0;
   
            break;
        case 2:
            xAdjust = 3;
            yAdjust = 5;
            xSourceAdjust = 0;
            ySourceAdjust = 3;
         
            break;
        case 3:
            xAdjust = 0;
            yAdjust = 8;
            xSourceAdjust = 3;
            ySourceAdjust = 3;
   
            break;
        default:

            break;
    }
    
    NSMutableArray * arrayCopy;
    arrayCopy = [[NSMutableArray alloc]initWithCapacity:6];
    for (int i=0; i<6; i++)
        [arrayCopy addObject: [[NSMutableArray alloc]initWithCapacity:6]];
    for (int j=0; j<6;j++)
        for (int i=0;i<6; i++) {
            arrayCopy[i][j] = self.gameBoard[i][j];
        }
    
    
    //Right Rotation
    if ([direction isEqualToString:@"right"]) {
        
    for (int y=0; y<3; y++) {
        for (int x=0; x<3; x++) {
            self.gameBoard[yAdjust-(y+ySourceAdjust)][(x+xSourceAdjust)+xAdjust] = arrayCopy[x+xSourceAdjust][y+ySourceAdjust];

            
        }
    }

    }
    //Left Rotation
    else
    {
        for (int y=0; y<3; y++) {
            for (int x=0; x<3; x++) {
                self.gameBoard[(-xAdjust)+(y+ySourceAdjust)][yAdjust-(x+xSourceAdjust)] = arrayCopy[x+xSourceAdjust][y+ySourceAdjust];
            }
        }

    }
    self.rotateMode = false;
}
-(NSString*) didWin
{
    //Counting like a pro
    //Love me some Nested Loops and Multidimensional arrays
    
    int p1HCount;
    int p2HCount;
    int p1VCount;
    int p2VCount;

    int p1DMRCount = 0;
    int p2DMRCount = 0;
    int p1DMLCount = 0;
    int p2DMLCount = 0;
    
    int p1DURCount = 0;
    int p2DURCount = 0;
    int p1DULCount = 0;
    int p2DULCount = 0;
    
    int p1DLRCount = 0;
    int p2DLRCount = 0;
    int p1DLLCount = 0;
    int p2DLLCount = 0;
    
    for (int y=0; y<6; y++) {
        p1HCount = 0;
        p2HCount = 0;
        p1VCount = 0;
        p2VCount = 0;
        
        for (int x=0; x<6; x++) {
            
                //Counting Horizontal
                if ([self.gameBoard[x][y] isEqual: @0]) {
                    p1HCount =0;
                    p2HCount =0;
                }
            
                else if([self.gameBoard[x][y] isEqual: @1]){
                    p1HCount++;
                    p2HCount = 0;
                }
        
                else{
                    p1HCount = 0;
                    p2HCount++;
                }
            
                //Counting Vertical
                if ([self.gameBoard[y][x] isEqual: @0]) {
                    p1VCount =0;
                    p2VCount =0;
                }
            
                else if([self.gameBoard[y][x] isEqual: @1]){
                    p1VCount++;
                    p2VCount = 0;
                }
            
                else{
                    p1VCount = 0;
                    p2VCount++;
                }
            
                //Counting Diagonals
                if(x==y)
                {
                    //Diagonal Middle Right counting
                    if ([self.gameBoard[x][y] isEqual: @0] && p1DMRCount<5 &&p2DMRCount<5) {
                      
                        p1DMRCount =0;
                        p2DMRCount =0;
                    }
                    
                    else if([self.gameBoard[x][y] isEqual: @1]&& p1DMRCount<5 &&p2DMRCount<5){
                        
                        p1DMRCount++;
                        p2DMRCount = 0;
                      
                    }
                    
                    else if([self.gameBoard[x][y] isEqual: @2]&& p1DMRCount<5 &&p2DMRCount<5){
                        p1DMRCount = 0;
                        p2DMRCount++;
                    }
                    //Diagonal Middle left counting
                    if ([self.gameBoard[5-x][y] isEqual: @0] && p1DMLCount<5 && p2DMRCount<5) {
                        
                        p1DMLCount =0;
                        p2DMLCount =0;
                    }
                    
                    else if([self.gameBoard[5-x][y] isEqual: @1]&& p1DMLCount<5 && p2DMRCount<5){
                        
                        p1DMLCount++;
                        p2DMLCount = 0;
                        
                    }
                    
                    else if([self.gameBoard[5-x][y] isEqual: @2]&& p1DMLCount<5 && p2DMRCount<5){
                        p1DMLCount = 0;
                        p2DMLCount++;
                    }
                }
            
            if ((x-y)==1) {
                //Diagonal Upper Right counting
                if([self.gameBoard[x][y] isEqual: @1]){
                    
                    p1DURCount++;

                    
                }
                
                else if([self.gameBoard[x][y] isEqual: @2]){
                    p2DURCount++;
                }
            }
            
            if ((x+y)==4) {
                //Diagonal Upper Left counting
                if([self.gameBoard[x][y] isEqual: @1]){
                    
                    p1DULCount++;
                    
                    
                }
                
                else if([self.gameBoard[x][y] isEqual: @2]){
                    p2DULCount++;
                }
            }
            
            if ((y-x)==1) {
                //Diagonal Lower Right counting
                if([self.gameBoard[x][y] isEqual: @1]){
                    
                    p1DLRCount++;
                    
                    
                }
                
                else if([self.gameBoard[x][y] isEqual: @2]){
                    p2DLRCount++;
                }
            }
            if ((x+y)==6) {
                //Diagonal Lower Left counting
                if([self.gameBoard[x][y] isEqual: @1]){
                    
                    p1DLLCount++;
                    
                    
                }
                
                else if([self.gameBoard[x][y] isEqual: @2]){
                    p2DLLCount++;
                }
            }
            //Returning for Horizontal and vertical wins
            if (p1HCount >=5 || p1VCount >=5) {
                self.gameWon = true;
                return @"Player 1 Wins";
            }
            else if (p2HCount >= 5 || p2VCount >=5){
                self.gameWon = true;
                return @"Player 2 Wins";
            }
            
        }

            
    }
    //Returning for Diagonal Wins
    if (p1DMRCount >=5 || p1DMLCount >=5 || p1DULCount == 5 || p1DURCount == 5 || p1DLLCount ==5 || p1DLRCount == 5) {
        
        self.gameWon = true;
        return @"Player 1 Wins";
    }
    else if (p2DMRCount >= 5 || p2DMLCount >=5 || p2DULCount == 5 || p2DURCount == 5 || p2DLLCount ==5 || p2DLRCount == 5){
        
        self.gameWon = true;
        return @"Player 2 Wins";
    }
    
    
    return @"Game In Progress";
}
@end

