//
//  PentagoSubBoardViewController.m
//  PentagoStudentVersion
//
//  
//  Copyright (c) 2014 Mark Griffth. All rights reserved.
//

#import "PentagoSubBoardViewController.h"
#import "PentagoBrain.h"

const int BORDER_WIDTH = 10;
const int TOP_MARGIN = 50;


@interface PentagoSubBoardViewController () {
    int subsquareNumber;
    int widthOfSubsquare;

}

@property (nonatomic, strong) PentagoBrain *pBrain;
@property (nonatomic, strong) UIImageView *gridImageView;
@property (nonatomic, strong) UITapGestureRecognizer *tapGest;
@property(nonatomic) UISwipeGestureRecognizer *rightSwipe;
@property(nonatomic) UISwipeGestureRecognizer *leftSwipe;

-(void) didTapTheView: (UITapGestureRecognizer *) tapObject;

@end

@implementation PentagoSubBoardViewController

-(void) killBrain
{
    [self.pBrain resetBrain];
}
-(UITapGestureRecognizer *) tapGest
{
    if( ! _tapGest ) {
        _tapGest = [[UITapGestureRecognizer alloc]
                    initWithTarget:self action:@selector(didTapTheView:)];
        
        [_tapGest setNumberOfTapsRequired:1];
        [_tapGest setNumberOfTouchesRequired:1];
    }
    return _tapGest;
}

-(PentagoBrain *) pBrain
{
    
    if( ! _pBrain )
    {
        
        _pBrain = [PentagoBrain sharedInstance];
    }
    return _pBrain;
}

-(UIImageView *) gridImageView
{
    if( ! _gridImageView ) {
        _gridImageView = [[UIImageView alloc] initWithFrame: CGRectZero];
    }
    return _gridImageView;
}


-(id) initWithSubsquare: (int) position
{
    // 0 1
    // 2 3
    if( (self = [super init]) == nil )
        return nil;
    subsquareNumber = position;
    // appFrame is the frame of the entire screen so that appFrame.size.width
    // and appFrame.size.height contain the width and the height of the screen, respectively.
    CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
    widthOfSubsquare = ( appFrame.size.width - 3 * BORDER_WIDTH ) / 2;
    
    return self;
}


-(void) didTapTheView: (UITapGestureRecognizer *) tapObject
{

    // p is the location of the tap in the coordinate system of this view-controller's view (not the view of the
    // the view-controller that includes the subboards.)
    CGPoint viewP = [tapObject locationInView:self.view];
    CGPoint p = [tapObject locationInView:self.gridImageView];
    //Blocking out of bounds placement
    if (p.x > (widthOfSubsquare -1) || p.y > (widthOfSubsquare -1)) {
        return;
    }
    int squareWidth = widthOfSubsquare / 3;
    
    //Placing within game
    int pXCoord = (int) (viewP.x / squareWidth);
    int pYCoord = (int) (viewP.y / squareWidth);
    BOOL turn = [self.pBrain p1Turn];
    if (![self.pBrain placeWithX:pXCoord AndY:pYCoord InQuad:subsquareNumber])
        return;

    // The board is divided into nine equally sized squares and thus width = height.
    UIImageView *iView;
    if (turn) {
        iView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"redMarble.png"]];
    }
    else
        iView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blueMarble.png"]];
    iView.frame = CGRectMake((int) (p.x / squareWidth) * squareWidth,
                             (int) (p.y / squareWidth) * squareWidth,
                             squareWidth+2 - (BORDER_WIDTH) / 3,
                             squareWidth+2 - (BORDER_WIDTH) / 3);
    //changed view to gridlayout
    [self.gridImageView addSubview:iView];

    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"winStatus"
     object:[self.pBrain didWin]];
    
  

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect ivFrame = CGRectMake(0, 0, widthOfSubsquare, widthOfSubsquare);
    self.gridImageView.frame = ivFrame;
    UIImage *image = [UIImage imageNamed:@"gridblack.png"];
    
    [self.gridImageView setImage:image];
    [self.gridImageView setBackgroundColor:[UIColor greenColor]];
    [self.view addSubview:self.gridImageView];
    [self.view addGestureRecognizer: self.tapGest];
    [self.view addGestureRecognizer: self.rightSwipe];
    [self.view addGestureRecognizer: self.leftSwipe];
    [self.view setBackgroundColor:[UIColor blackColor]];
  
   CGRect viewFrame = CGRectMake( (BORDER_WIDTH + widthOfSubsquare) * (subsquareNumber % 2) + BORDER_WIDTH,
                                  (BORDER_WIDTH + widthOfSubsquare) * (subsquareNumber / 2) + BORDER_WIDTH + TOP_MARGIN,
                                  widthOfSubsquare, widthOfSubsquare-20);
        self.view.frame = viewFrame;
   
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"winStatus"
     object:@"Game in Progress"];
    
}

-(UISwipeGestureRecognizer *) rightSwipe
{
    
    if( !_rightSwipe ) {
        _rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
        [_rightSwipe setDirection:UISwipeGestureRecognizerDirectionRight];
    }
    return _rightSwipe;
}
-(UISwipeGestureRecognizer *) leftSwipe
{
    
    if( !_leftSwipe ) {
        _leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
        [_leftSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
    }
    return _leftSwipe;
}
-(void) didSwipe: (UISwipeGestureRecognizer *) swipeObject
{
    if (!self.pBrain.isRotateMode  || self.pBrain.gameIsWon) {
        return;
    }
 
    if (swipeObject.direction == UISwipeGestureRecognizerDirectionRight) {
        
    
    CGAffineTransform currTransform = self.gridImageView.layer.affineTransform;
    [UIView animateWithDuration:1 animations:^ {
        CGAffineTransform newTransform = CGAffineTransformConcat(currTransform, CGAffineTransformMakeRotation(M_PI/2));
        self.gridImageView.layer.affineTransform = newTransform;
        [self.pBrain swipedDirection:@"right" inQuad:subsquareNumber];

    }];
    

    
    [self.view bringSubviewToFront:self.gridImageView];

   
    }
    if (swipeObject.direction == UISwipeGestureRecognizerDirectionLeft) {
        
        
        CGAffineTransform currTransform = self.gridImageView.layer.affineTransform;
        [UIView animateWithDuration:1 animations:^ {
            CGAffineTransform newTransform = CGAffineTransformConcat(currTransform, CGAffineTransformMakeRotation(-(M_PI/2)));
            self.gridImageView.layer.affineTransform = newTransform;
            [self.pBrain swipedDirection:@"left" inQuad:subsquareNumber];
            
        }];
        
        
        

        
        
    }
    [self.view bringSubviewToFront:self.gridImageView];
    [self.view addGestureRecognizer:self.leftSwipe];
    [self.view addGestureRecognizer:self.rightSwipe];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"winStatus"
     object:[self.pBrain didWin]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
