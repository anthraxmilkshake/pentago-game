//
//  PentagoSubBoardViewController.h
//  PentagoStudentVersion
//
//
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PentagoSubBoardViewController : UIViewController
-(id) initWithSubsquare: (int) position;
-(void) killBrain;

@end

