//
//  PentagoBrain.h
//  PentagoStudentVersion
//
//  
//  Copyright (c) 2014 Mark Griffith All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PentagoBrain : NSObject

+(PentagoBrain *) sharedInstance;
-(BOOL) placeWithX: (int) x AndY: (int) y InQuad: (int) q;
-(void) swipedDirection: (NSString*)direction inQuad:(int)quad;
-(BOOL) p1Turn;
-(BOOL) isRotateMode;
-(void) resetBrain;
-(BOOL) gameIsWon;
-(NSString*) didWin;
@end
